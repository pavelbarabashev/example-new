import { Button } from "@mui/material";
import { useEffect, useState } from "react";
import "./App.css";
import { Users } from "./components/Users";
import { UserContext } from "./Helpers/Contexts";
import { User } from "./Helpers/Types";

function App() {
  let [data, setData] = useState(Array<User>);
  let [isUsersViewVisible, setIsUsersViewVisible] = useState(false);
  let users = [
    { name: "Name1", age: 40 },
    { name: "Name2", age: 41 },
    { name: "Name3", age: 42 },
    { name: "Name4", age: 43 },
    { name: "Name5", age: 44 },
    { name: "Name6", age: 45 },
  ];

  function getData() {
    return Promise.resolve(users);
  }

  useEffect(() => {
    function getData() {
      return Promise.resolve(users);
    }

    getData().then((resolvedData) => {
      setData(resolvedData);
    });
  }, []);
  return (
    <div className="screen-center">
      <Button
        onClick={() => {
          setIsUsersViewVisible(true);
        }}
      >
        Show users
      </Button>
      <UserContext.Provider
        value={{ users: data, setState: setIsUsersViewVisible }}
      >
        {isUsersViewVisible ? <Users /> : <></>}
      </UserContext.Provider>
    </div>
  );
}

export default App;
