import React, { useContext, useState } from "react";
import { UserContext } from "../Helpers/Contexts";
import { Button, Pagination } from "@mui/material";
export const Users: React.FC = () => {
  let [item, setItem] = useState(1);
  let { users, setState } = useContext(UserContext);
  return (
    <>
      <Button
        onClick={() => {
          setState(false);
        }}
      >
        Hide users
      </Button>
      <Pagination
        page={item}
        onChange={(_, num) => {
          setItem(num);
        }}
        count={users.length}
        color="primary"
      />
      <div>
        <p>Name: {users[item - 1].name}</p> <p>Age: {users[item - 1].age}</p>
      </div>
    </>
  );
};
