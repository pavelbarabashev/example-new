import React, { createContext, SetStateAction } from "react";
import { User } from "./Types";

type ContextType = {
  users: Array<User>;
  setState: React.Dispatch<SetStateAction<boolean>>;
};

export const UserContext = createContext<ContextType>({
  users: [],
  setState: () => {},
});
